package com.flytxt.neondx.utility;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.annotation.PreDestroy;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import lombok.extern.slf4j.Slf4j;

/**
 * The ParquetFileMergeJob class
 *
 * @author Meenakshi
 *
 */

@Slf4j
public class ParquetFileMergeJob implements CommandLineRunner {

    private static String LOCATION;

    private static int THREAD_POOL_SIZE;

    private static boolean THROTTLING_ENABLED;

    private static int MAX_FILE_COUNT_LIMIT;

    private static int MAX_FILE_SIZE_IN_MB;

    private static int MERGE_PERIOD;

    private static final String MERGE_INPUT_DIR = "mergeInputDir";

    private static final String MERGE_DIR = "mergeDir";

    private static final String MERGED_PARQUET = "_merged.parquet";

    private static Configuration conf;

    private static ThreadPoolExecutor executor = null;

    private final static List<Future<?>> FUTURES = new ArrayList<Future<?>>();

    public static void main(final String[] args) throws IOException {
        LOCATION = System.getProperty("storageLocation", "/data/neon/events");
        THREAD_POOL_SIZE = Integer.valueOf(System.getProperty("threadPoolSize", "3")).intValue();
        THROTTLING_ENABLED = Boolean.valueOf(System.getProperty("throttlingEnabled", "true"));
        MAX_FILE_COUNT_LIMIT = Integer.valueOf(System.getProperty("maxFileCountLimit", "100000")).intValue();
        MAX_FILE_SIZE_IN_MB = Integer.valueOf(System.getProperty("maxFileSizeInMb", "100")).intValue();
        MERGE_PERIOD = Integer.valueOf(System.getProperty("mergePeriod", "105")).intValue();
        SpringApplication.run(ParquetFileMergeJob.class, args);
    }

    @Override
    public void run(final String... args) throws Exception {
        init();
    }

    public static void init() throws IOException {
        conf = HBaseConfiguration.create();
        createThreadPoolExecutor();
        FUTURES.clear();

        log.info("Intialized parquet file merger|executors:{}|throttle:{}|fileCountLimit:{}|fileSizeLimit:{}mb", THREAD_POOL_SIZE, THROTTLING_ENABLED, MAX_FILE_COUNT_LIMIT, MAX_FILE_SIZE_IN_MB);
        mergeParquetFiles();
    }

    private static void createThreadPoolExecutor() {
        executor = new ThreadPoolExecutor(THREAD_POOL_SIZE, THREAD_POOL_SIZE, 60000L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>(Integer.MAX_VALUE), new RejectedExecutionHandler() {

            @Override
            public void rejectedExecution(final Runnable r, final ThreadPoolExecutor executor) {
                try {
                    log.debug("ParquetFileMergeJob|Executor pool full. Waiting....");
                    executor.getQueue().put(r);
                    log.debug("ParquetFileMergeJob|Executor pool free. Submitted request....");
                } catch (final Throwable e) {
                    log.error("ParquetFileMergeJob|Submit to pool interupted. Rejecting request: ", e);
                    throw new RuntimeException(e);
                }
            }
        });
        executor.allowCoreThreadTimeOut(true);
    }

    public static void mergeParquetFiles() throws IOException {

        try {

            final FileSystem fs = FileSystem.get(conf);
            final Path source = new Path(LOCATION);
            final Path destination = new Path(LOCATION);

            boolean runsuccessful = false;
            long startTime = 0;

            while (!runsuccessful) {
                try {
                    startTime = System.currentTimeMillis();
                    log.info("Merging iteration started");

                    iterateDirectories(source, fs, destination);

                    boolean allTasksCompleted = false;
                    while (!allTasksCompleted) {
                        allTasksCompleted = true;
                        for (final Future<?> future : FUTURES) {
                            allTasksCompleted &= future.isDone();
                        }
                    }
                    runsuccessful = true;
                    log.info("Merging iteration completed, TT: {}ms", (System.currentTimeMillis() - startTime));

                } catch (final Exception e) {
                    log.error("Error on merging parquet files ", e);
                }
                if (!runsuccessful) {
                    Thread.sleep(1000);
                }
            }
        } catch (final Exception e) {
            log.error("Error on merging parquet files", e);
        } finally {
            shutdownExecutor();
        }
    }

    private static void iterateDirectories(final Path source, final FileSystem fs, final Path outputLocation) throws IOException {
        final FileStatus[] fileStatuses = fs.listStatus(source);
        for (final FileStatus filestatus : fileStatuses) {

            if (filestatus.isDirectory() && !(filestatus.getPath().getName().equals(MERGE_INPUT_DIR)) && !(filestatus.getPath().getName().equals(MERGE_DIR))) {
                final Path f = new Path(outputLocation, filestatus.getPath().getName());
                checkAndCreateDirectories(fs, f);
                iterateDirectories(filestatus.getPath(), fs, f);
            } else if (!(folderOlderThanMergePeriod(filestatus.getPath().getParent().getName(), MERGE_PERIOD))) {
                executeMergeProcess(filestatus.getPath().getParent(), fs, outputLocation);
                return;
            }
        }
    }

    private static boolean checkAndCreateDirectories(final FileSystem fs, final Path p) throws IOException {
        if (!fs.exists(p)) {
            fs.mkdirs(p);
            return false;
        }
        return true;
    }

    private static boolean folderOlderThanMergePeriod(final String folderName, final long mergePeriod) {
        final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
        final LocalDate today = LocalDate.now();
        final LocalDate date = LocalDate.parse(folderName, formatter);
        final long noOfDaysBetween = ChronoUnit.DAYS.between(date, today);

        return noOfDaysBetween > mergePeriod;
    }

    private static void executeMergeProcess(final Path parent, final FileSystem fs, final Path outputLocation) {
        final String fileNameIdentifier = parent.getName() + "_" + System.currentTimeMillis();
        final ParquetMergeWorker finalMerge = new ParquetMergeWorker(parent, fs, outputLocation, fileNameIdentifier, conf, THROTTLING_ENABLED, MAX_FILE_COUNT_LIMIT, MAX_FILE_SIZE_IN_MB);
        final Future<?> f = executor.submit(finalMerge);
        FUTURES.add(f);
    }

    @PreDestroy
    public void destroy() {
        shutdownExecutor();
    }

    private static void shutdownExecutor() {
        if (executor == null) {
            return;
        }

        boolean isExecutorShutdown = false;
        while (!isExecutorShutdown) {
            try {
                if (!executor.isShutdown()) {
                    executor.shutdown();
                }
                if (!executor.isTerminated()) {
                    executor.awaitTermination(120, TimeUnit.SECONDS);
                }
                isExecutorShutdown = true;
                log.info("Completed shutdown of the executor");
            } catch (final Exception e) {
                log.error("Failed while waiting for executor shutdown, will retry indefinitely error: ", e);
            }
        }
        executor = null;
    }

}
