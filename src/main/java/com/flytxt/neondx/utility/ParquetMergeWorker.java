package com.flytxt.neondx.utility;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.parquet.hadoop.ParquetFileWriter;
import org.apache.parquet.hadoop.metadata.FileMetaData;

import lombok.extern.slf4j.Slf4j;

/**
 * The ParquetMergeWorker class
 *
 * @author Meenakshi
 *
 */
@Slf4j
public class ParquetMergeWorker implements Runnable {

    private final Configuration conf;

    private static final String MERGE_INPUT_DIR = "mergeInputDir";

    private static final String MERGE_DIR = "mergeDir";

    private static final String PARQUET_SUCCESS = ".parquet.success";

    private static final String PARQUET_TMP = ".parquet.tmp";

    private static final String PARQUET = ".parquet";

    private static final String MERGED_PARQUET = "_merged.parquet";

    private final Path parent;

    private final FileSystem fs;

    private final Path outputLocation;

    private final String fileNameIdentifier;

    private boolean throttlingEnabled = true;

    private final int fileCountLimit;

    private final long fileSizeLimitInBytes;

    private final List<Path> inputFilesList = new ArrayList<Path>();

    private long totalFileSizeInBytes = 0;

    private int fileIndexForMoving;

    public ParquetMergeWorker(final Path parent, final FileSystem fs, final Path outputLocation, final String fileNameIdentifier, final Configuration conf, final boolean throttlingEnabled,
            final int fileCountLimit, final int fileSizeLimitInMb) {
        this.parent = parent;
        this.fs = fs;
        this.outputLocation = outputLocation;
        this.fileNameIdentifier = fileNameIdentifier;
        this.conf = conf;
        this.throttlingEnabled = throttlingEnabled;
        this.fileCountLimit = fileCountLimit;
        this.fileSizeLimitInBytes = (fileSizeLimitInMb * 1024 * 1024);
    }

    @Override
    public void run() {
        try {
            log.info("Merging started|{}", parent);
            final long t = System.currentTimeMillis();

            init();

            processPendingFiles();

            processNewFiles();

            clear();

            log.info("Merging completed|{}|TT:{}", parent, (System.currentTimeMillis() - t));

        } catch (final Exception e) {
            log.error("Error on merging parquet files @ " + parent, e);
        }
    }

    private void processNewFiles() throws IOException {
        final FileStatus[] files = fs.listStatus(parent);

        final Path inputDir = new Path(parent, MERGE_INPUT_DIR);
        boolean createInputDir = true;

        for (final FileStatus file : files) {
            if (!validFile(file.getPath().toString())) {
                continue;
            }

            if (createInputDir) {
                checkAndCreateDirectories(inputDir);
                createInputDir = false;
            }

            // move file to merge input directory
            final Path renamedPath = new Path(inputDir, file.getPath().getName());
            fs.rename(file.getPath(), renamedPath);

            inputFilesList.add(renamedPath);
            totalFileSizeInBytes += file.getLen();

            if (isThrottleLimitReached()) {
                processInputFiles();
                createInputDir = true;
            }
        }
        // process remaining files if any
        processInputFiles();

        // successfully completed merging of all files, delete the residues
        checkAndDeleteDirectory(new Path(parent, MERGE_DIR));
    }

    private void processInputFiles() throws IOException {
        if (inputFilesList.isEmpty()) {
            return;
        }

        log.debug("Merging files|{}|{}", parent, inputFilesList.size());

        final Path mergeFile = mergeParquetFiles();

        checkAndDeleteDirectory(new Path(parent, MERGE_INPUT_DIR));

        moveSuccessFileToOutput(mergeFile);

        clear();
    }

    private void processPendingFiles() throws Exception {
        final Path mergeDir = new Path(parent, MERGE_DIR);

        if (fs.exists(mergeDir)) {
            final FileStatus[] mergeDirStatus = fs.listStatus(mergeDir);

            if (mergeDirStatus.length > 0) {
                final Path mergeFile = mergeDirStatus[0].getPath();

                if (mergeFile.getName().endsWith(PARQUET_SUCCESS)) {
                    checkAndDeleteDirectory(new Path(parent, MERGE_INPUT_DIR));
                    moveSuccessFileToOutput(mergeFile);
                    checkAndDeleteDirectory(mergeDir);

                } else if (mergeFile.getName().endsWith(PARQUET_TMP)) {
                    log.info("Incomplete file found|{}|{}", parent, mergeFile);
                    fs.delete(mergeFile, true);
                    addPendingFilesToInputFileList();
                }
            } else {
                addPendingFilesToInputFileList();
            }
        }

        if (isThrottleLimitReached()) {
            // this can happen when previous run failed due to high memory and the limit has been reduced
            processInputFiles();
        }
    }

    private void clear() {
        inputFilesList.clear();
        totalFileSizeInBytes = 0;
    }

    private void init() {
        clear();
    }

    private boolean isThrottleLimitReached() {
        return throttlingEnabled && (totalFileSizeInBytes >= fileSizeLimitInBytes || inputFilesList.size() >= fileCountLimit);
    }

    private boolean checkAndCreateDirectories(final Path p) throws IOException {
        if (!fs.exists(p)) {
            fs.mkdirs(p);
            return false;
        }
        return true;
    }

    private void moveSuccessFileToOutput(final Path src) throws IOException {
        Path renamedPath = null;
        do {
            renamedPath = new Path(outputLocation, fileNameIdentifier + "_" + fileIndexForMoving + MERGED_PARQUET);
            fileIndexForMoving++;
        } while (fs.exists(renamedPath));

        fs.rename(src, renamedPath);
    }

    private void checkAndDeleteDirectory(final Path directoryToDelete) throws IOException {
        if (fs.exists(directoryToDelete)) {
            fs.delete(directoryToDelete, true);
        }
    }

    private boolean validFile(final String s) {
        return ((!s.endsWith(MERGED_PARQUET)) && (s.endsWith(PARQUET)));
    }

    private Path mergeParquetFiles() throws IOException {
        final Path finalMerge = new Path(parent, MERGE_DIR);
        checkAndCreateDirectories(finalMerge);

        final Path intermediatePath = new Path(finalMerge, fileNameIdentifier + PARQUET_TMP);

        final FileMetaData mergedMeta = ParquetFileWriter.mergeMetadataFiles(inputFilesList, conf).getFileMetaData();
        final ParquetFileWriter writer = new ParquetFileWriter(conf, mergedMeta.getSchema(), intermediatePath, ParquetFileWriter.Mode.CREATE);

        writer.start();
        for (final Path input : inputFilesList) {
            writer.appendFile(fs.getConf(), input);
        }
        writer.end(mergedMeta.getKeyValueMetaData());

        final Path finalPath = new Path(finalMerge, fileNameIdentifier + PARQUET_SUCCESS);
        fs.rename(intermediatePath, finalPath);

        return finalPath;
    }

    private void addPendingFilesToInputFileList() throws IOException {
        final Path inputDir = new Path(parent, MERGE_INPUT_DIR);

        if (!fs.exists(inputDir)) {
            return;
        }

        final FileStatus[] pendingFiles = fs.listStatus(inputDir);

        for (final FileStatus file : pendingFiles) {
            if (isThrottleLimitReached()) {
                // this can happen when previous run failed due to high memory and the limit has been reduced
                // So moving file to parent folder
                fs.rename(file.getPath(), new Path(parent, file.getPath().getName()));
            } else {
                inputFilesList.add(file.getPath());
                totalFileSizeInBytes += file.getLen();
            }
        }
        log.info("Found pending files|{}|{}|{}", parent, pendingFiles.length, inputFilesList.size());
    }
}
